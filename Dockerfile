FROM nginx
COPY . /export/files/phoenix
COPY conf.d/phoenix.conf /etc/nginx/conf.d/
COPY cert/* /etc/nginx/cert/
EXPOSE 443
LABEL registry.cn-shanghai.aliyuncs.com/bearhunting-fe/phoenix=helong.feng