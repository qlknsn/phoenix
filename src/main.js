import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// import 'magic.css/dist/magic.min.css'
// import { TimeSelect } from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'

import { DatetimePicker } from 'vant';
import 'vant/lib/index.css';

// import weui from 'weui.js'
import weui from 'weui.js'
import "weui";
Vue.prototype.$weui = weui

// Vue.prototype.$weui = weui
import Storage from 'vue-ls';
 
var options = {
  namespace: 'vuejs__', // key键前缀
  name: 'ls', // 命名Vue变量.[ls]或this.[$ls],
  storage: 'local', // 存储名称: session, local, memory
};
 
Vue.use(Storage, options);

// Vue.use(Switch)
// Vue.use(TimeSelect)

Vue.use(DatetimePicker);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
