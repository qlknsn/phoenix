import Vue from "vue";
import VueRouter from "vue-router";
import radarRouter from "./radarRouter";

Vue.use(VueRouter);

const routes = [

  // {
  //   path: "/",
  //   name: "RADAR-HOMEPAGE",
  //   component: () => import("../views/radar/login/login.vue"),
  // },
  {
    path: "/",
    name: "RADAR-HOMEPAGE",
    component: () => import("../views/radar/homepage"),
  },
  {
    path: "/radar",
    name: "RADAR-PROGRAM",
    component: () => import("../views/radar/radarhomepage"),
  },
  ...radarRouter,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
