const radarRouter = [
  {
    path: "/radar/login",
    name: "RADAR-LOGIN",
    meta: { title: "安安上海" },
    component: () => import("../views/radar/login/login.vue"),
  },
  {
    path: "/radar/dashboard",
    name: "dashboard",
    meta: { title: "安安E管家" },
    component: () => import("../views/radar/dashboard/dashboard.vue"),
  },
  {
    path: "/radar/personal",
    name: "personal",
    meta: { title: "信息查询" },
    component: () => import("../views/radar/info/infoSearch.vue"),
  },
  {
    path: "/radar/autolockList",
    name: "autolockList",
    meta: { title: "自动锁列表" },
    component: () => import("../views/radar/dashboard/autoUnlockList.vue"),
  },
  {
    path: "/radar/lockSetting",
    name: "LOCKSETTING",
    meta: { title: "加锁设置" },
    component: () => import("../views/radar/lock/lockSetting.vue"),
  },
  {
    path: "/radar/editLock",
    name: "EDITLOCK",
    meta: { title: "编辑锁" },
    component: () => import("../views/radar/lock/editLock.vue"),
  },
  {
    path: "/radar/upInfo",
    name: "upInfo",
    meta: { title: "登记雷达信息" },
    component: () => import("../views/upInfo/Home.vue"),
  },
  {
    path: "/radar/radarlogin",
    name: "radarlogin",
    meta: { title: "安装人员登录" },
    component: () => import("../views/upInfo/login.vue"),
  },
  {
    path: "/radar/upuserInfo",
    name: "upuserInfo",
    meta: { title: "登记人信息" },
    component: () => import("../views/upInfo/bindUser.vue"),
  },
  {
    path: "/radar/shipment",
    name: "shipment",
    meta: { title: "登记人信息" },
    component: () => import("../views/upInfo/shipment.vue"),
  },
  {
    path: "/radar/selectBtnPage",
    name: "selectBtnPage",
    meta: { title: "功能选择页" },
    component: () => import("../views/upInfo/selectBtnPage.vue"),
  },
];
export default radarRouter;
