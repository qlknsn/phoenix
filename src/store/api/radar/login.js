import api from "./loginUrl";
import { axios } from "@/utils/requests";
export function login(data) {
  return axios({
    url: api.login,
    data: data,
    // params: data,            
    method: "post",
  });
}
export function getOpenId(params) {
  return axios({
    url: api.getOpenId,
    params: params,
  });
}

export function bindUser(params) {
  return axios({
    url: api.bindUser,
    // params: params,
    data: params,
    method: "post",
  });
}

export function getBikeList(params) {
  return axios({
    url: api.getBikeList,
    data: params,
    method:'post'
  });
}

export function updateBike(data) {
  return axios({
    url: api.updateBike,
    data,
    method: "post",
  });
}
export function lock(data) {
  return axios({
    url: api.lock,
    params:data,
    method: "post",
  });
}
export function getlockList(data) {
  return axios({
    url: api.getlockList+data.bikeUrn,
    method: "get",
  });
}
export function editlockConfig(data) {
  return axios({
    url:`${api.editlockConfig}/${data[0].bikeUrn}`,
    data:data,
    method: "post",
  });
}
export function unbindUser(data) {
  return axios({
    url: api.unbindUser,
    params:data,
    method: "post",
  });
}
export function getbikeInfo(data) {
  return axios({
    url: `${api.getbikeInfo}/${data.key}`,
    params:data,
  });
}
export function districtSearch(data) {
  return axios({
    url: `${api.districtSearch}?start=${data.start}&count=${data.count}`,
    data:{parents:data.parents},
    method: "post",
  });
}
export function jsapiSignature(data) {
  return axios({
    url: api.jsapiSignature,
    params:data,
  });
}
export function fileUpload(data) {
  let timestamp=new Date().getTime()
  return axios({
    method:'post',
    url: `${api.fileUpload}?urn=${timestamp}`,
    data:data,
  });
}
export function radarEntities(data) {
  // let timestamp=new Date().getTime()
  return axios({
    method:'post',
    url: `${api.radarEntities}?action=update`,
    data:data,
  });
}
export function radarEntitieskey(data) {
  // let timestamp=new Date().getTime()
  return axios({
    url: `${api.radarEntitieskey}/${data.key}`,
  });
}
