const api = {
  // 登录
  login: "/sphyrnidae/v1/subscriber/login",
  getOpenId: "/sphyrnidae/v1/wechat/openid/mp",
  bindUser: "/sphyrnidae/v1/subscriber/wxBind",
  unbindUser: "/sphyrnidae/v1/subscriber/wxUnBind",
  getBikeList: "/sphyrnidae/v1/bike/search",
  updateBike: "/sphyrnidae/v1/bicycleOwner/updateBicycleById",
  lock: "/sphyrnidae/v1/bike/lock",
  editlockConfig: "/sphyrnidae/v1/bike/locker",
  getlockList: "/sphyrnidae/v1/bike/locker/",
  getbikeInfo: "/sphyrnidae/v1/bike/entity",
  // 获取区域接口
  
  districtSearch:'/sphyrnidae/v1/district/search',
  // 获取jssdk
  jsapiSignature:'/sphyrnidae/v1/wechat/jsapiSignature',
  // districtSearch:'/sphyrnidae/v1/district/search',
  // 上传图片
  // https://radar-test-backend.bearhunting.cn/public/api/sphyrnidae/v1/bike/image/upload?urn=
  fileUpload:'/sphyrnidae/v1/bike/image/upload',
  // 更新雷达设备
  radarEntities:'/sphyrnidae/v1/radar/entities',
  // 查询雷达信息
  radarEntitieskey:'/sphyrnidae/v1/radar/entity',

};
export default api;
