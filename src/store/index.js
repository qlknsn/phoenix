import Vue from "vue";
import Vuex from "vuex";
import radar from '@/store/module/radar/radar'
Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    radar
  }
})
