import {
  getBikeList,
  login,
  getOpenId,
  bindUser,
  updateBike,
  lock,
  getlockList,
  editlockConfig,
  unbindUser,
  districtSearch,
  jsapiSignature,
  fileUpload,
  radarEntities,
  radarEntitieskey,
  getbikeInfo
} from "../../api/radar/login";
import { Response } from "@/utils/response";
import store from "@/store/index";
import Vue from "vue"
import {
  LOGIN,
  BIND_USER,
  GET_OPEN_ID,
  GET_BIKE_LIST,
  UPDATE_BIKE,
  MODIFY_TIME_LIST,
  SET_TIME_LIST,
} from "@/store/mutation-types/radar-mutations";
import route from "@/router/index";
const state = {
  userInfo: "",
  openId: "",
  timeList: [],
  bikeList: [
    {
      bikeEntity: {
        auditHead: { createTime: "" },
        productModel: { brandName: "" },
        licensePlate: "",

      },
      properties: { lockStatus: "" },
      snapshots: {
        'LEFT': [{ url: '' }],
        'FRONT': [{ url: '' }],
        'BACK': [{ url: '' }],
        'LICENSE': [{ url: '' }],
        'SIN_FRONT': [{ url: '' }],
        'SIN_BACK': [{ url: '' }],
        'ENGINE': [{ url: '' }],
        'VIN': [{ url: '' }],
      },
      productModel: {
        brandName: ''
      },
      licensePlate: '',
      auditHead: {
        createTime: ''
      }
    },
  ],
  getlockList: {},
  radarUserInfo: {},
  districtList: [],
  ticketObject: {},
  filePaths: [],
  toastVisible: false,
  toastInfo: "",
  toastSuccess: false,
  singleRadarInfo: {}
};
const actions = {
  getbikeInfo({ commit }, data) {
    let p = getbikeInfo(data);
    p.then((res) => {
      commit("GET_BIKE_INFO", res);
    });
  },
  updateBike({ commit }, data) {
    let p = updateBike(data);
    p.then((res) => {
      commit("UPDATE_BIKE", res);
    });
  },
  getBikeList({ commit }, data) {
    let p = getBikeList(data);
    p.then((res) => {
      commit("GET_BIKE_LIST", res);
    });
    return p;
  },
  login({ commit }, data) {
    let p = login(data);
    p.then((res) => {
      commit("LOGIN", res);
    });
  },
  radarlogin({ commit }, data) {
    let p = login(data);
    p.then((res) => {
      console.log(res)
      commit("RADAR_LOGIN", res);
    });
  },
  getOpenId({ commit }, data) {
    let p = getOpenId(data);
    p.then((res) => {
      commit("GET_OPEN_ID", res);
    });
    return p;
  },
  bindUser({ commit }, data) {
    let p = bindUser(data);
    p.then((res) => {
        commit("BIND_USER", res);
    });
  },
  bindradarUser({ commit }, data) {
    let p = bindUser(data);
    p.then((res) => {
      if(res.error ==null){
        commit("BIND_RADAR_USER", res);
      }else{
        alert('未查到该手机号,请核查改手机号是否在后台录入')
      }
      
    });
  },
  lock({ commit }, data) {
    commit;
    let p = lock(data);
    return p;
    // p.then((res) => {
    //   commit("BIND_USER", res);
    // });
  },
  getlockList({ commit }, data) {
    commit;
    let p = getlockList(data);
    p.then((res) => {
      commit("GET_LOCKS_LIST", res);
    });
    return p;
  },
  editlockConfig({ commit }, data) {
    commit;
    let p = editlockConfig(data);
    // p.then((res) => {
    //   commit("GET_LOCKS_LIST", res);
    // });
    return p;
  },
  unbindUser({ commit }, data) {
    commit;
    let p = unbindUser(data);
    return p;
  },
  districtSearch({ commit }, params) {
    store.commit("CHANGE_TOASTVISIBLE", true);
    store.commit("CHANGE_TOASTINFO", "正在加载数据请稍后");
    let p = districtSearch(params);
    p.then(res => {
      store.commit("CHANGE_TOASTVISIBLE", false);
      commit('GET_DISTRICT', res.results)
    })
  },
  districtchildSearch({ commit }, params) {
    // store.commit("CHANGE_TOASTVISIBLE", true);
    // store.commit("CHANGE_TOASTINFO", "正在加载数据请稍后");
    let p = districtSearch(params);
    return p;
    // p.then(res => {
    //   // store.commit("CHANGE_TOASTVISIBLE", false);
    //   commit('GET_CHILD_DISTRICT', {data:res.results,index:params.index})
    // })
  },
  jsapiSignature({ commit }, params) {
    let p = jsapiSignature(params);
    p.then((res) => {
      commit;
      commit("GET_TICKET", res);
      console.log(res)
    });
  },
  fileUpload({ commit }, data) {
    commit;

    store.commit("CHANGE_TOASTVISIBLE", true);
    store.commit("CHANGE_TOASTINFO", "图片上传中，请稍等");
    let p = fileUpload(data);
    p.then(res => {
      if (res.error == null) {
        store.commit('CHANGE_TOASTVISIBLE', false)
        store.commit('CHANGE_TOASTINFO', '')
        commit('GET_FILE_LIST', res)
        // alert(res.result)
      } else {
        alert(res.error)
        store.commit('CHANGE_TOASTINFO', '图片上传失败请重试')
        setTimeout(() => {
          store.commit('CHANGE_TOASTVISIBLE', false)
        }, 1000);
      }
    })
  },
  radarEntities({ commit }, data) {
    commit;
    let p = radarEntities(data);
    return p;
  },
  radarEntitieskey({ commit }, data) {

    commit;
    store.commit("CHANGE_TOASTVISIBLE", true);
    store.commit("CHANGE_TOASTINFO", "正在查询此设备...")
    let p = radarEntitieskey(data);
    p.then(res => {
      if (res.error) {
        store.commit("CHANGE_TOASTINFO", "该设备没有入库,请联系设备管理人员")
        setTimeout(() => {
          store.commit("CHANGE_TOASTVISIBLE", false);
        }, 1500);
      } else {
        store.commit("CHANGE_TOASTINFO", "已在数据库查到该设备")
        setTimeout(() => {
          store.commit("CHANGE_TOASTVISIBLE", false);
        }, 1500);
        // store.commit("CHANGE_TOASTVISIBLE", false);
        commit('GET_RADAR_INFO', res)
      }

    })
  },

};

const mutations = {
  CHANGE_SINGLERADARINFO(state, res) {
    state.singleRadarInfo = res
  },
  GET_BIKE_INFO(state, res) {
    state.singlebikeInfo = res.result
  },
  CHANGE_RADARINFO(state, res) {
    state.singleRadarInfo.auditHead.lastUpdater = Vue.ls.get('memberUrn')
    state.singleRadarInfo[res.name] = res.value
  },
  GET_RADAR_INFO(state, res) {
    state.singleRadarInfo = res.result
  },
  CHANGE_TOASTVISIBLE(state, res) {
    state.toastVisible = res
  },
  CHANGE_TOASTINFO(state, res) {
    state.toastInfo = res
  },
  CHANGE_TOASTSUCCESS(state, res) {
    state.toastSuccess = res
  },
  GET_FILE_LIST(state, res) {
    state.filePaths.push(res.result)
    let uploaderInput = document.getElementById('uploaderInput')
    uploaderInput.value = ''
  },
  GET_FILE(state, res) {
    state.filePaths = res
  },
  DELETE_IMG(state, index) {
    state.filePaths.splice(index, 1)
  },
  GET_TICKET(...arg) {
    let [state, { result }] = [...arg];
    state.ticketObject = result;
  },
  GET_DISTRICT(state, data) {
    data.forEach(item => {
      item.label = item.name
      item.value = item.urn
      // let par = {
      //   parents: [item.urn],
      //   start: 0,
      //   count: 100,
      //   // index:index
      // }
      // store.dispatch('districtchildSearch', par).then(res => {
      //   res.results.forEach(i => {
      //     i.label = i.name
      //     i.value = i.urn
      //   })
      //   item.children = res.results
      // })

    })
    console.log(data)
    state.districtList = data
  },
  // GET_CHILD_DISTRICT(state,data){
  //   data.data.forEach(item => {
  //     item.label = item.name
  //     item.value = item.urn
  //   })
  //   state.districtList[data.index].children = data.data
  // },
  [MODIFY_TIME_LIST](state, data) {
    state.timeList.push(data);
  },
  [SET_TIME_LIST](state, data) {
    state.timeList = data;
  },
  [UPDATE_BIKE](...arg) {
    let [state, { results, error }] = [...arg];
    if (Response.error(error)) {
      error;
      store.dispatch("getBikeList");
    } else {
      alert("接口请求失败");
      state;
      results;
    }
  },
  [GET_BIKE_LIST](...arg) {
    let [state, { results }] = [...arg];
    if (results.length == 0) {
      alert("您的名下没有电动车.");
      state.bikeList = [
        {
          bikeEntity: {
            auditHead: { createTime: "" },
            productModel: { brandName: "" },
            licensePlate: "",
          },
          properties: { lockStatus: "" },
          snapshots: {
            'LEFT': [{ url: '' }],
            'FRONT': [{ url: '' }],
            'BACK': [{ url: '' }],
            'LICENSE': [{ url: '' }],
            'SIN_FRONT': [{ url: '' }],
            'SIN_BACK': [{ url: '' }],
            'ENGINE': [{ url: '' }],
            'VIN': [{ url: '' }],
          },
          productModel: {
            brandName: ''
          },
          licensePlate: '',
          auditHead: {
            createTime: ''
          }
        },
      ];
    } else if (results.length > 0) {
      // results.forEach(item=>{
      //   item.LEFT = item.snapshots.LEFT
      // })
      state.bikeList = results;
    } else {
      alert("出错啦");
    }
  },
  [BIND_USER](...arg) {
    let [state, { error, result }] = [...arg];
    state;
    result;
    if (result) {
      state.userInfo = result;
      Vue.ls.set("token", result.auth.token);
      Vue.ls.set("memberUrn", result.properties.account.member.urn);
      Vue.ls.set("userAccountUrn", result.properties.account.urn);
      route.push("/radar/dashboard");
    } else if (error.code == 500) {
      console.log(error);
      // alert(error.message);
      window.location.href = process.env.VUE_APP_AUTH_URL;
    }
  },
  BIND_RADAR_USER(...arg) {
    let [state, { result }] = [...arg];
    state;
    result;
    if (result.auth) {
      state.userInfo = result;
      if (result.auth.token) {
        Vue.ls.set("token", result.auth.token);
      }
      if (result.properties?.account?.member) {
        Vue.ls.set("memberUrn", result.properties.account.member.urn);
        Vue.ls.set("userAccountUrn", result.properties.account.urn);
      }
      route.push("/radar/selectBtnPage");
      // route.push("/radar/login");
      // window.location.href = process.env.VUE_APP_DO_MAIN + '/radar/upInfo';
    } else {
      
      // setTimeout(() => {
        window.location.href = process.env.VUE_APP_RADAR_URL;
      // }, 2000);
      
    }
  },
  [GET_OPEN_ID](...arg) {
    arg;
    let [state, { error, result }] = [...arg];
    state;
    if (error) {
      if (error.code == 500) {
        // window.location.href = process.env.VUE_APP_AUTH_URL;
      }
    } else {
      sessionStorage.setItem("openId", result.openId);
      state.openId = result.openId;
    }
  },
  RADAR_LOGIN(...arg) {
    console.log('雷达安装')
    let [, { error, result }] = [...arg];
    if (error) {
      error;
      route.push("/radar/radarlogin");
    } else {
      result;
      // if (result.auth) {
      //   state.radarUserInfo = result;
      //   Vue.ls.set("radartoken", result.auth.token);
      //   Vue.ls.set("radarmemberUrn", result.properties.account.member.urn);
      //   route.push("/radar/upInfo");
      // } else {
      route.push("/radar/radarlogin");
    }
    // }
  },
  [LOGIN](...arg) {
    let [, { error, result }] = [...arg];
    if (error) {
      // alert(error.msg)
    } else {
      if (result.auth?.token) {
        state.userInfo = result;
        // Vue.ls.set("token", result.auth.token);
        // Vue.ls.set("memberUrn", result.properties.account.member.urn);
        if (result.auth?.token) {
          Vue.ls.set("token", result.auth.token);
        }
        if (result.properties?.account?.member) {
          Vue.ls.set("memberUrn", result.properties.account.member.urn);
          Vue.ls.set("userAccountUrn", result.properties.account.urn);
        }
        route.push("/radar/dashboard");
      } else {
        route.push("/radar/login");
      }
    }
  },
  GET_LOCKS_LIST(state, res) {
    state.getlockList = res;
  }
};

export default {
  state,
  actions,
  mutations,
};
