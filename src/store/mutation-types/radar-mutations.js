export const USER_INFO = "USER_INFO";
export const GET_OPEN_ID = "GET_OPEN_ID";
export const BIND_USER = "BIND_USER";
export const GET_BIKE_LIST = "GET_BIKE_LIST";
export const UPDATE_BIKE = "UPDATE_BIKE";
export const LOGIN = "LOGIN";
export const MODIFY_TIME_LIST = "MODIFY_TIME_LIST";
export const SET_TIME_LIST = "SET_TIME_LIST";
