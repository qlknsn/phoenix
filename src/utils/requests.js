import Vue from "vue";
import axios from "axios";
// import router from "../router/index.js";
import { VueAxios } from "./axios";
// import { config } from "./config.js";
const CancelToken = axios.CancelToken;
let requestList = [];
// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // api base_url
  timeout: 600000, // 请求超时时间
  cancelToken: new CancelToken(function(e) {
    requestList.push(e);
  }),
});

const err = (error) => {
  if (error.response) {
    console.log(error.response)
    if (error.response.status === 401) {
      console.log("401...");
      error;
    }
    if (error.response.status == 404) {
      // Vue.prototype.$message({
      //   type: "error",
      //   message: "API未找到",
      // });
      alert('API未找到')
    }
    if (error.response.status === 400) {
      // console.log(router.currentRoute.name)
      // Vue.prototype.$message({
      //   type: "error",
      //   message: "接口请求错误。",
      // });
      alert(error.response.data.message)
    }
    if (error.response.status === 403) {
      // notification.error({
      //     message: 'Forbidden',
      //     description: data.message
      // })
      alert("forbidden");
    }
    if (error.response.status === 503) {
      // console.log(error.response)
      // console.log.error("Bad GateWay")
      alert('系统错误')
    }
    if (error.response.status === 500||error.response.status === 502) {
      // Vue.prototype.$message({
      //   type: "error",
      //   // message: error.response.data.message
      //   message: "系统错误",
      // });
      alert('系统错误')
    }
  }
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use((config) => {
  // if(!config.url.indexOf("v1/subscriber/login") && !config.url.indexOf("v1/wechat/openid/mp")){
      
  // }
  const token = Vue.ls.get('token');
      if (token) {
          config.headers['token'] = token;
      }
  config.headers['Access-Control-Allow-Origin'] = '*'
  return config;
}, err);

// response interceptor
service.interceptors.response.use((response) => {
  return response.data;
}, err);

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service);
  },
};

export { installer as VueAxios, service as axios };
