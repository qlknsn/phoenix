export class Response {
  constructor(error) {
    this.error = error;
  }
  static error(error) {
    if (error == null) {
      //   说明是null值,直接返回false
      return false;
    }
    if (error.length > 0) {
      // 说明是数组
      let mark = true;
      error.forEach((errors) => {
        mark = mark && errors.code == 200;
      });
      return mark;
    } else {
      //   说明是对象
      if (error.code == 200) {
        //   判断code值，200为返回正确
        return true;
      } else {
        return false;
      }
    }
  }
}
