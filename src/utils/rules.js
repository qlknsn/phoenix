// 验证手机号码
export function checkPhoneNumber(rule, value, callback) {
  const reg = /^((0\d{2,3}-\d{7,8})|(1[3|4|5|6|7|8|9]\d{9}))$/;
  if (!value) {
    return callback(new Error("请填写手机号码！"));
  } else if (!reg.test(value)) {
    return callback(new Error("请填写正确的手机号码！"));
  } else {
    callback();
  }
}

export function checkIdNum(rule, value, callback) {
  const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  if (!value) {
    return callback(new Error("证件号码不能为空"));
  } else if (!reg.test(value)) {
    return callback(new Error("证件号码不正确"));
  } else {
    callback();
  }
}

export function checkEmpty(rule, value, callback) {
  if (!value) {
    return callback(new Error("输入内容不能为空"));
  }
}

export function isvalidUsername(str) {
  var reg = /^[0-9A-Za-z]{3,32}$/;
  return reg.test(str);
}

export default {
  checkPhone: [
    { required: false, validator: checkPhoneNumber, trigger: "blur" }
  ],
  checkId: [{ required: true, validator: checkIdNum, trigger: "blur" }],
  checkEmpty: [{ required: true, validator: checkEmpty, trigger: "blur" }],
  checkName: [{ required: true, validator: isvalidUsername, trigger: "blur" }]
};
