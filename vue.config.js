const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  outputDir: process.env.NODE_ENV === "production" ? `phoenix-pro` : "phoenix",
  chainWebpack: (config) => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("assets", resolve("src/assets"))
      .set("components", resolve("src/components"))
      .set("public", resolve("public"));
  },
  configureWebpack: {
    externals: {
      AMap: "AMap",
    },
    resolve: {
      alias: {
        vue$: "vue/dist/vue.esm.js",
      },
    },
    module: {
      rules: [
        {
          test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
          loader: "file-loader",
        },
      ],
    },
  },
  devServer: {
    port:8088,
    disableHostCheck: true,
    proxy: {
      "/sphyrnidae": {
        // target: "https://radar-backend.bearhunting.cn/public/api/sphyrnidae",
        target: "https://radar.bearhunting.cn/sphyrnidae",
        // target: "http://192.168.63.161:9001/public/api/sphyrnidae",
        changeOrigin: true,
        pathRewrite: {
          "^/sphyrnidae": "",
        },
      },
    },
  },
  
};
